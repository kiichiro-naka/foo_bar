Ext.define('FooBar.controller.Books', {
	extend: 'Ext.app.Controller',
	requires: [
		'Ext.tab.Panel',
		'FooBar.controller.SomeViewController'
	],
	stores: [
        'Books',
    ],
    storesInitializing: 0,
    loadStores: function(fn) {
    	var me = this
    	var stores = Ext.Array.map(me.stores, function(store) {
    		return (me['get' + store + 'Store'].call(me))
    	})
		Ext.globalEvents.addListener('idle', function() {
			if (me.storesInitializing == 0) {
				me.storesInitializing = stores.length
				var listener = arguments.callee
				Ext.Array.each(stores, function(store) {
					store.load({
						callback: function(records, operation, success) {
							Ext.globalEvents.removeListener('idle', listener)
							if ((-- me.storesInitializing) == 0) {
								Ext.callback(fn)
							}
						}
					})
				})
			}
		})
	},
	init: function() {
		var me = this
		me.callSuper(arguments)
		me.loadStores(function() { console.log("Done") })
		// // --
		// var initializing = false
		// var initialized = false
		// Ext.globalEvents.addListener('idle', function() {
		// 	if (! initializing && ! initialized) {
		// 		var listener = arguments.callee
		// 		store = me.getBooksStore()
		// 		store.load({
		// 			callback: function(records, operation, success) {
		// 				debugger
		// 				Ext.globalEvents.removeListener('idle', listener)
		// 				initialized = true
		// 				initializing = false
		// 				console.log("Initialized")
		// 			}
		// 		})
		// 		initializing = true
		// 	}
		// 	console.log("Called")
		// })
		// // --

		// --

		me.control({
			'booksview': {
				containerclick: function(dataview, e, eOpts) {
					target = e.getTarget()
					tag = target.tagName
					type = target.getAttribute('type')
					command = target.getAttribute('command')
					if (tag == 'INPUT' && type == 'button') {
						if        (command == 'refresh_dataview') {
							dataview.store.reload()
						} else if (command == 'add_filter') {
							me.getBooksStore().addFilter([
								{
									property: 'isbn',
									value: /978-4-7/,
								},
							])
						} else if (command == 'clear_filter') {
							me.getBooksStore().clearFilter()
						} else if (command == 'sort_by_id') {
							me.getBooksStore().sort([{ property: 'id', direction: 'ASC', }])
						} else if (command == 'sort_by_isbn') {
							me.getBooksStore().sort([{ property: 'isbn', direction: 'ASC', }])
						} else if (command == 'sort_by_title') {
							me.getBooksStore().sort([{ property: 'title', direction: 'ASC', }])
						} 
					}
				},
				itemclick: function(dataview, record, item, index, e, eOpts) {
					target = e.getTarget()
					tag = target.tagName
					type = target.getAttribute('type')
					command = target.getAttribute('command')
					child_index = parseInt(target.getAttribute('child_index'))
					//debugger;
					if (tag == 'INPUT' && type == 'button') {
						if        (command == 'delete_book') {
							dataview.store.remove(record)
							dataview.store.sync()
							// dataview.refreshNode(child_index) // 自動的に反映される
						} else if (command == 'delete_review') {
							var reviews = record.reviews()
							reviews.removeAt(child_index)
							reviews.sync()
							dataview.refresh(record)
							/// Associations のストアへの変更は、dataview へは伝播しない模様
							dataview.refreshNode(index)
						}
					}
				},
			},
		})
	},
});

						// if        (button_id == 'book') {
						// 	isbn = record.get('isbn')
						// 	console.log('book: ' + isbn)
						// 	record.set('isbn', isbn + ' X')
						// 	component.refreshNode(index)
						// } else if (button_id == 'review') {
						// 	review_id = el.getAttribute('review_id')
						// 	console.log('review: ' + record.get('isbn') + ' の ' + review_id)
						// }
						// // debugger
