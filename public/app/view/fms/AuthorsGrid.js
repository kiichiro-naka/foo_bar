Ext.define('FooBar.view.AuthorsGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.authors_grid',
    title: 'Authors Pane',
    store: 'Authors',
    columns: [
        { header: 'Name',    dataIndex: 'name',    flex: 1 },
        { header: 'Address', dataIndex: 'address', flex: 1 },
    ],
})
