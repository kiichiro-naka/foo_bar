Ext.define('FooBar.store.Authors', {
    extend: 'Ext.data.Store',
    requires: [
    	'Ext.data.Request',
        'Ext.data.association.HasOne',
    ],
    model: 'FooBar.model.Author',
    // data: [
    //     { name: 'Hoge Fuga', address: 'Address 1' },
    //     { name: 'Foo Bar',   address: 'Address 2' },
    // ],
    proxy: {
        type: 'ajax',
        url: '/authors.json',
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'authors',
        },
    },
    autoLoad: true,
});
