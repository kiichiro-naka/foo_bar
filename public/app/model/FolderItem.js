Ext.define('FooBar.model.fms.FolderItem', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id', type: 'string', },
        { name: 'name', type: 'string', },
        { name: 'leaf', type: 'boolean', },
    ],
    proxy: {
        type: 'rest',
        url: '/nodes',
        format: 'json',
        reader: {
            type: 'json',
            root: 'children',
        },
    },
});
