Ext.define('FooBar.controller.SomeViewController', {
	extend: 'Ext.app.Controller',
	stores: [ 'Authors', ],
	refs: [
		{ ref: 'hoge', selector: '#hoge' },
		{ ref: 'someView', selector: 'someView' },
		{ ref: 'dynContainer', selector: '#dynContainer' }
	],
	init: function() {
		this.callSuper(arguments)
		this.control({
			'#someView': {
				'itemclick': function(grid, record, item, index, e, eOpts) {
					// debugger
					cnt = this.getDynContainer()
					cnt.removeAll()
					cnt.add(Ext.widget('panel'), {
						region: 'center',
						html: 'Hello ' + record.get('name') ,
						// destroy: function() {
						// 	//this.callSuper(arguments)
						// 	console.log('destroyed')
						// }
					})
				},
			},
		})
	},
});
