Ext.define('FooBar.model.Author', {
	extend: 'Ext.data.Model',
	requires: [
		'FooBar.model.User',
		'Ext.data.association.Association',
		'Ext.data.association.HasOne',
	],
	fields: [ 
		{ name: 'id',      type: 'int' },
		{ name: 'name',    type: 'string', },
		{ name: 'address', type: 'string', },
		{ name: 'user_id', type: 'int' },
	],
	//hasOne: { model: 'FooBar.model.User', associatedName: 'User', foreignKey: 'user_id' },
	associations: [
		{
			type: 'hasOne',
			//associatedName: '', // 任意の名前をつけられる
			getterName: 'getHogeUser',
			model: 'FooBar.model.User',
			primaryKey: 'id', // 先のデータの主キー。こっちの foreignKey と突き合わせる
			foreignKey: 'user_id', // ネストデータが存在しなければ、これに従って読みに行く？
			//associationKey: 'user', // データ内のサブデータを読みに行くキー
		},
	],
});
