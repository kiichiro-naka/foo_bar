Ext.define("FooBar.view.BooksView", {
    extend: 'Ext.view.View',
    alias: 'widget.booksview',
    //requires: [
    // //uses: [
    //   'FooBar.store.Books',
    // ],
    // store: Ext.data.StoreManager.lookup('FooBar.store.Books'),
    //store: Ext.create('FooBar.store.Books'),
    store: 'Books', // Application.js 等で作っておけば、OK
    tpl: new Ext.XTemplate((function(){/*
<p>
  <input type=button value=refresh command=refresh_dataview>
  <input type=button value=new command=new_book>
  <input type=button value='add filter' command=add_filter>
  <input type=button value='clear filter' command=clear_filter>
  <input type=button value='sort by id' command=sort_by_id>
  <input type=button value='sort by isbn' command=sort_by_isbn>
  <input type=button value='sort by title' command=sort_by_title>
  <!-- <input type=button value='' command=> -->
<ul>
  <tpl for=".">
    <div class="book-wrap">
      <li>
        {isbn}「{title}」
        <input type=button value=edit   command=edit_book>
        <input type=button value=delete command=delete_book>
      </li>
      <ul>
      <tpl for="reviews">
        <li>
          {body}
          <!-- XTempalte の添字は 0 オリジンなことに注意 -->
          <input type=button value=edit   command=edit_review   child_index={# - 1}>
          <input type=button value=delete command=delete_review child_index={# - 1}>
        </li>
      </tpl>
      </ul>
    </div>
  </tpl>
</ul>
*/}).toString().match(/\n([\s\S]*)\n/)[1] ),
    itemSelector: 'div.book-wrap',

	// listeners: {
	// 	click: {
	// 		element: 'li',
	// 		fn: function(dataview, index, node, e) {
	// 			console.log('hoge')
	// 		},
	// 	},
	// },
    autoScroll: true,
    emptyText: 'No books available',
    // data: [ 
    //     { src:'http://www.sencha.com/img/20110215-feat-drawing.png', caption:'Drawing & Charts' },
    //     { src:'http://www.sencha.com/img/20110215-feat-data.png', caption:'Advanced Data' },
    //     { src:'http://www.sencha.com/img/20110215-feat-html5.png', caption:'Overhauled Theme' },
    //     { src:'http://www.sencha.com/img/20110215-feat-perf.png', caption:'Performance Tuned' },
    // ],
    initComponent: function() {
    	this.callSuper(arguments)
    	// this.update([
	    //     { src:'http://www.sencha.com/img/20110215-feat-drawing.png', caption:'Drawing & Charts' },
	    //     { src:'http://www.sencha.com/img/20110215-feat-data.png', caption:'Advanced Data' },
	    //     { src:'http://www.sencha.com/img/20110215-feat-html5.png', caption:'Overhauled Theme' },
	    //     { src:'http://www.sencha.com/img/20110215-feat-perf.png', caption:'Performance Tuned' },
    	// ])
    },
});


	 //    '<ul>',
		//     '<tpl for=".">',
		//         '<div class="thumb-wrap">',
		//           '<li>{#}. {isbn} <input type=button button_id=book value=button></li>',
		//           '<ul>',
		//             '<tpl for="reviews">',
	 //                  '<li>{#}. {body} <input type=button button_id=review review_id={id} value=button></li>',
		//             '</tpl>',
		//           '</ul>',
		//         '</div>',
		//     '</tpl>',
		// '</ul>'
// '\
// 	<ul>\
// 	    <tpl for=".">\
// 	        <div class="thumb-wrap">\
// 	          <li>{#}. {isbn} <input type=button button_id=book value=button></li>\
// 	          <ul>\
// 	            <tpl for="reviews">\
// 	              <li>{#}. {body} <input type=button button_id=review review_id={id} value=button></li>\
// 	            </tpl>\
// 	          </ul>\
// 	        </div>\
// 	    </tpl>\
// 	</ul>\
// '
