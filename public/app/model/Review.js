Ext.define('FooBar.model.Review', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'body', type:'string' },
    ],
    proxy: {
        type: 'rest',
        url: '/reviews',
        format: 'json',
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'books',
        },
    },
});
