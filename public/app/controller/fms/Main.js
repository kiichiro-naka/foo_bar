Ext.define('FooBar.controller.fms.Main', {
	extend: 'Ext.app.Controller',
	views: [
		'fms.RootTree',
		'fms.FolderGrid',
	],
	stores: [
		'fms.TreeNodes',
		'fms.FolderItems',
	],
	init: function() {
		var me = this
		me.callSuper(arguments)
		me.control({
			'roottree': {
				itemclick: function(treepanel, record, item, index, e, eOpts) {
					me.getFmsFolderItemsStore().load({
						params: {
							node: record.get('id'),
						} 
					})
				},
			},
		})
		//debugger
	},
});
