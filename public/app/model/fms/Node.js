Ext.define('FooBar.model.Node', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'name', type: 'string', },
        { name: 'leaf', type: 'boolean', },
        { name: 'id', type: 'string', }, // "id" を持つと、"node" で Web API に聞く。何だそりゃ
    ],
    proxy: {
        type: 'rest',
        url: '/nodes',
        reader: 'json',
        format: 'json',
        extraParams: {
            noleaf: true,
        }
    },
});
