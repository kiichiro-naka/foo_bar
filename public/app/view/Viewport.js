Ext.define('FooBar.view.Viewport', {
    extend: 'Ext.container.Viewport',
    requires: [
        //'Ext.layout.container.Fit',
        //'Ext.grid.Panel',
        'Ext.data.Request',
        'FooBar.view.Main',
        'FooBar.view.AuthorsGrid',
        'FooBar.view.AnotherView',
        'FooBar.view.SomeView',
        'FooBar.view.FilerView',
    //],
    //uses: [
       'FooBar.view.BooksView',
    ],
    layout: {
        type: 'border',
        // Ext.layout.container.Border - Ext JS 4.2.0 - Sencha Docs http://localhost:1841/docs/#!/api/Ext.layout.container.Border
        regionWeights: {
            west:    20,
            east:    10,
            center:   0,
            north:  -10,
            south:  -20,
        },
    },
    defaults: {
        collapsible: true,
        split: true,
    },
    items: [
        {
            region: 'west',
            width: '20%',

            xtype: 'grid',
            itemId: 'funcs',
            title: 'Function Selector',
            columns: [
                { header: 'Name', dataIndex: 'name', flex: 1 },
            ],
            store: {
                fields: [ 'name', ],
                // ここへ、タブ内に生成するビュー名を列挙
                data: [
                    { name: 'SomeView', },
                    { name: 'AnotherView', },
                    { name: 'BooksView', },
                    { name: 'FilerView' },
                ],
            },
            hideHeaders: true,
        },
        {
            region: 'center',
            xtype: 'tabpanel',
            itemId: 'mainTabPanel',
            title: 'Tab Panel',
            items: [
                // {
                //     html: 'foobar',
                // },
                // {
                //     html: 'bazbaz',
                // },
            ],
        },
        // {
        //     region: 'north',
        //     layout: 'fit',
        //     height: '50%',

        //     xtype: 'authors_grid',
        // },
        // {
        //     region: 'center',
        //     floatable: false,

        //     xtype: 'form',
        //     itemId: 'author_pane',
        //     title: 'Author Pane',
        //     items: [
        //         {
        //             xtype: 'textfield',
        //             name : 'name',
        //             fieldLabel: 'Name'
        //         },
        //         {
        //             xtype: 'textfield',
        //             name : 'address',
        //             fieldLabel: 'Address'
        //         }
        //     ],
        // },
    ],
    initComponent: function() {
        this.callSuper(arguments)
        // debugger
    },
});
