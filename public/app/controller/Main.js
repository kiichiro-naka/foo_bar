Ext.define('FooBar.controller.Main', {

	requires: [
		'Ext.tab.Panel',
		'FooBar.controller.SomeViewController'
	],
	extend: 'Ext.app.Controller',
	stores: [ 'Authors', ],
	refs: [
		{ ref: 'authorPane', selector: '#author_pane' },
		{ ref: 'tabPanel', selector: '#mainTabPanel' },
	],
	controllers: [
		'SomeViewController',
		'Books',
		'fms.Main',
	],
	doControl: function() {
		this.control({
			'viewport > #funcs': {
				'itemclick': function(grid, record, item, index, e, eOpts) {
					tabs = this.getTabPanel()
					var name = record.get('name')
					var tab = tabs.items.findBy(function(item, key) {
						return (item.title == name)
					})
					if (! tab) {
						tab = tabs.add(Ext.create('FooBar.view.' + name, {
							title: name,
						}))
					}
					tabs.setActiveTab(tab);
					// tabs.items.getAt(1)
					// tabs.items.get('aaa')
					// console.log("Hello")
					//debugger
				},
			},
			'viewport > authors_grid': {
				'itemdblclick': function(
					grid, 
					record, /* model インスタンス */
					item,   /* dom エレメント */
					index,  /* 0 インデックス */
					e,      /* Ext.EventObject */
					eOpts   /* ？ */ ) {
					//debugger;
					// u1 = record.getHogeUser()
					record.getHogeUser({
					    reload: true, // force a reload if the owner model is already cached
					    callback: function(user, operation) {console.log('cp1')}, // a function that will always be called
					    success : function(user, operation) {console.log('cp2: ' + user.get('email'))}, // a function that will only be called if the load succeeded
					    failure : function(user, operation) {console.log('cp3')}, // a function that will only be called if the load did not succeed
					    scope   : this // optionally pass in a scope object to execute the callbacks in
					});

					u2 = FooBar.model.User.load(2, {
					    success: function(user) {
					        console.log('email: ' + user.get('email'));
					    },
					    failure: function(user) {
					      console.log('failure')
					    },
					});

					this.getAuthorPane().loadRecord(record)
					// record = form.getRecord()
					// record.getData()
					// values = form.getValues()
					// record.set(values);
					
				}
			},
		})
	},
	init: function() {
		this.callSuper(arguments)
		this.doControl()
	},
});
