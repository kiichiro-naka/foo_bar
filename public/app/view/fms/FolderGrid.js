Ext.define('FooBar.view.fms.FolderGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.foldergrid',
    title: 'Folder Grid',
    store: 'fms.FolderItems',
    columns: [
        {
            header: 'Name', 
            dataIndex: 'name', 
            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                return (record.get('leaf')? value: value + "/")
            },
            columnWidth: '100%',
            width: '100%',
        },
    ],
    initComponent: function() {
        // debugger
        this.callSuper(arguments)
    }
})
