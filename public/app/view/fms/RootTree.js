Ext.define('FooBar.view.fms.RootTree', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.roottree',
    title: 'Root Tree',
    store: 'fms.TreeNodes',
    rootVisible: false,
    columns: [
        {
        	xtype: 'treecolumn',
        	header: 'Name',
        	dataIndex: 'name',
        	flex: 1,
        },
    ],
	hideHeaders: true,
})
