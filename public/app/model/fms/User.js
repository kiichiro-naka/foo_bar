Ext.define('FooBar.model.User', {
    extend: 'Ext.data.Model',
    fields: [
    	{ name: 'id',       type: 'int' },
    	{ name: 'username', type: 'string' },
    	{ name: 'email',    type: 'string' },
    ],
    proxy: {
        type: 'ajax',
        url: '/users.json',
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'users',
        },
    },
    autoLoad: true,
});
