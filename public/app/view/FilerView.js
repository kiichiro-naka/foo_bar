Ext.define('FooBar.view.FilerView', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.layout.container.Border', // border
        'Ext.tab.Panel', // tabpanel
        'FooBar.view.fms.RootTree',
        'FooBar.view.fms.FolderGrid',
    ],
    layout: {
        type: 'border',
        regionWeights: {
            west:    20,
            east:    10,
            center:   0,
            north:  -10,
            south:  -20,
        },
    },
    defaults: {
        collapsible: true,
        split: true,
    },
    items: [
        {
            region: 'west',
            width: '20%',
            xtype: 'roottree',
        },
        {
            region: 'north',
            height: '50%',
            xtype: 'foldergrid',
        },
        {
            region: 'center',
            //
            xtype: 'tabpanel',
            itemId: 'mainTabPanel',
            title: 'Tab Panel',
            items: [
            ],
        },
    ],
    initComponent: function() {
        // debugger
        this.callSuper(arguments)
    },
});
