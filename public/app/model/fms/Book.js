Ext.define('FooBar.model.Book', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'id', type: 'integer', },
        { name: 'isbn', type: 'string', },
        { name: 'title', type: 'string', }
    ],
    requires: [ 'FooBar.model.Review' ], // 必須。Association インスタンス生成時に肝心のクラスが無いと、しらーと霧視されるっぽい
    // hasMany: {
    //     model: 'Review',
    //     name : 'reviews',
    // },
	associations: [
		{
			type: 'hasMany',
            model: 'FooBar.model.Review',
			associatedName: 'Review', // 任意の名前をつけられる
			//getterName: 'getHogeReview',
			//name: 'reviews',
            //ownerModel: 'FooBar.model.Book',
            //model: 'Review',
			//primaryKey: 'id', // 先のデータの主キー。こっちの foreignKey と突き合わせる
			//foreignKey: '_id', // ネストデータが存在しなければ、これに従って読みに行く？
			//associationKey: 'reviews', // データ内のサブデータを読みに行くキー
		},
	],
    proxy: {
        type: 'rest',
        url: '/books',
        format: 'json',
        reader: {
            type: 'json',
            successProperty: 'success',
            root: 'books',
        },
    },
});
