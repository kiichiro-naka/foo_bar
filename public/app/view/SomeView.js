Ext.define("FooBar.view.SomeView", {
    extend: 'Ext.container.Container',
    alias: 'widget.someView',
    layout: 'border',
    items: [
    	{
    		region: 'north',
    		height: '50%',

    		xtype: 'grid',
		    itemId: 'someView',
		    columns: [
		        { header: 'Name', dataIndex: 'name', flex: 1 },
		    ],
		    store: {
		        fields: [ 'name', ],
		        data: [
		            { name: 'foo', },
		            { name: 'bar', },
		            { name: 'baz', },
		        ],
		    },
		},
    	{
    		region: 'center',

    		xtype: 'container',
    		itemId: 'dynContainer',
    		layout: 'border',
    		autoDestroy: true,
    	},
    ],
});