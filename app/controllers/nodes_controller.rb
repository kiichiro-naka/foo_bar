require 'base64'

class NodesController < ApplicationController
  def index
    show
  end
  def show
    noleaf = false
    if params.key?(:noleaf) && params[:noleaf] == "true"
      noleaf = true
    end
    if (not params.key?(:node)) || params[:node] == "root"
      path = '/usr'
    else
      path = Base64.urlsafe_decode64(params[:node])
    end
    puts "*** #{path}"
    nodes = []
    Dir::entries(path).each { |item|
      if item[0] == "."
        next
      end
      item_path = "#{path}/#{item}"
      next if not File.exists?(item_path)
      next if noleaf and File.file?(item_path)
      nodes.push({
        :name => item,
        :id => Base64.urlsafe_encode64(item_path),
        :leaf => File.file?(item_path),
      })
    }
    # nodes.push({
    #   :name => 'bbb',
    #   :id => Base64.urlsafe_encode64("#{path}/bbb"),
    #   :leaf => false,
    # })
    dat = {
      :success => true,
      :children => nodes,
    }
    respond_to do |format|
      format.json { render(:json => dat.to_json) }
    end
  end
end
